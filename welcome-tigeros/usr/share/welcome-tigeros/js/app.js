$(document).ready(function(){

    $("#div-tor").on("mouseover mouseout", function(){
      $(".bubble").toggle();
    });

    $(".btn-install").click(function(e){
      e.preventDefault();
      $.get("./install.sh", this.value, function(){
        location.reload(true);
      });

    });

    $(".btn-remove").click(function(e){
        e.preventDefault();
        $.get("./remove.sh", this.value, function(){
          location.reload(true);
      });
      console.log(this.value);
    });

    $("#customSwitch1").on("change", function(){
        $.get("./autostart.sh");
    });

    $(".custom-control-label").mousedown(function(){
        $(this).css("background-color", "#fff");
    });

    $(".inputColor").click(function(e){
      e.preventDefault();
      $.get("./themes.sh", this.value, function(){
        location.reload(true);
      });
    });

});
